# demo-ui-tests-cypress
UI automation demo with Cypress

```sh
# open cypress GUI
npx cypress open

# run tests
npm run test

# generate reports
npm run report
```
